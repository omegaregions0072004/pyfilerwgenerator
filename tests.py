import unittest, sys
from modules.FileHandler import FileHandler

class TestsBadInput(unittest.TestCase):
    def testNoArgs(self):
        fh = FileHandler('bad_path', 'bad_path')
        self.assertEqual(fh.readAndWrite(), False)

    def emptyInputFile(self):
        fh = FileHandler('./io/inputFile_empty.txt', './io/outputFile.txt')
        self.assertEqual(fh.readAndWrite(), False)
        
class TestsOkInput(unittest.TestCase):
    def filePathsOk(self):
        fh = FileHandler('./io/inputFile.txt', './io/outputFile.txt')
        self.assertEqual(fh.readAndWrite(), True)

if __name__=='__main__':
    suite = unittest.TestSuite()
    suite.addTest(TestsBadInput('testNoArgs'))
    suite.addTest(TestsBadInput('emptyInputFile'))
    suite.addTest(TestsOkInput('filePathsOk'))
    unittest.TextTestRunner().run(suite)
