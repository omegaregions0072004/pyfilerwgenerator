import os

class FileHandler:
    def __init__(self, inputFilePath, outputFilePath) -> None:
        self.inputFilePath = inputFilePath
        self.outputFilePath = outputFilePath

    def readAndWrite(self) -> None:
        # Check in input file paths is valid, output will be created if it does not exist
        if not os.path.exists(self.inputFilePath): 
            print(__name__, 'Invalid input at ', self.inputFilePath)
            return False

        try: 
            with open(self.inputFilePath, 'r') as input, open(self.outputFilePath, 'w') as output:
                # Read data and split data by ',', remove whitespaces and empty strings
                lines = [ line.strip() for line in input.read().split(',') if line ] 
                lineCount = len(lines)
                if (lineCount == 0): 
                    print(__name__, ': Empty input data')
                    return False
                print(__name__, 'Total number of lines in input file: %s' % str(lineCount))

                # Save data in generator
                dataGen = self.dataGenerator(lines)

                # Print data from generator
                outputLineCount = 0
                for datum in dataGen: 
                    output.write('%s%s' % (datum, '\n'))
                    outputLineCount += 1
                print(__name__, 'Total number of lines in output file: %s' % str(outputLineCount))
                return True
        except Exception as e:
            print(__name__, ': Error opening io files')
            return False

    def dataGenerator(self, lines):
        yield from ( ('%s %s' % (str(i), str(line.lower()))) for (i, line) in enumerate(lines) )


